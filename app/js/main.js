// coding by Friso NL frisog - at gmail.com - Learning Javascript 001


window.onload = function() {

    // array of items of list + add click picLink event (show hide elements)
    var names = ["simon", "bruce", "ben"];
    var n = 0;
    for (n = 0; n < names.length; n++) {
        tempName = window[names[n]];
        tempName = document.getElementById(names[n]);
        tempName.addEventListener("click", picLink);
    };


    function picLink() {
        var allImages = document.querySelectorAll("img");
        var i = 0;
        for (i = 0; i < allImages.length; i++) {
            allImages[i].className = "";
            allImages[i].className = "hide";
        };


        var picId = this.attributes["data-img"].value;

        var pic = document.getElementById(picId);
        if (pic.className === "hide") {
            pic.className = "";
        } else {
            pic.className = "hide";
            return true;
        }
        return true;


    };

    function addDeleteButton(inputLi) {
        var deleteButton = document.createElement("input");
        deleteButton.className = "deleteButton";
        deleteButton.type = "submit";
        deleteButton.value = "delete";
        deleteButton.addEventListener("mousedown", removeItem);
        inputLi.appendChild(deleteButton);
    }

    // checklist code - add edit update functie (and also enter function.)

    var checklist = document.getElementById("checklist");
    var items = checklist.querySelectorAll("li");
    var inputs = checklist.querySelectorAll("input");
    var luList = checklist.querySelectorAll("lu");




    console.log(items);
    // loop to add eventlisterners and addDeleteButtons
    for (i = 0; i < items.length; i++) {
        items[i].addEventListener("click", editItem);
        inputs[i].addEventListener("blur", updateItem);
        inputs[i].addEventListener("keypress", itemKeypress);
        addDeleteButton(items[i]);
    }

    function removeItem() {
        this.parentNode.remove();
    }

    function editItem() {
        this.className = "edit";
        var input = this.querySelector("input");
        input.focus();
        input.setSelectionRange(0, input.value.length);

    }

    function updateItem() {
        this.previousElementSibling.innerHTML = this.value;
        this.parentNode.className = "";
        findSelectorforselectImageAccordingToLetter(this.value, this.parentNode);
    }

    function itemKeypress(event) {
        var event = event || window.event //For IE
        if (event.which === 13) {
            updateItem.call(this);
        };
    }



    // ******* after this make a link to a mysql database so that the list is stored!







    // add element function + end in edit mode for this new addition.

    document.getElementById("addButton").onclick = function(addLi) {
        // Create a <li> node
        // Create a text node


        var dataString = "Voer een gegeven in:";
        var dateSpan = document.createElement('span');
        var dataInput = document.createElement('input');
        var dataImg = document.createElement('img');
        dataImg.src = "img/apples.jpg"
        dataImg.id = "";
        dateSpan.innerHTML = dataString;
        dataInput.value = dataString;
        dataInput.addEventListener("blur", updateItem);
        dataInput.addEventListener("keypress", itemKeypress);
        var addLi = document.createElement("LI");


        addLi.appendChild(dateSpan);
        addLi.appendChild(dataInput);
        addLi.appendChild(dataImg);
        addDeleteButton(addLi);
        addLi.addEventListener("click", editItem); // Append the text to <li>
        checklist.appendChild(addLi);
        addLi.className = "edit";
        editItem.call(addLi);


    };

    var zeroletters = [];
    var oneletters = [];
    var twoletters = [];
    // sort alphabet in 3 arrays.
    function divAlphabet() {
        var alphabet = "abcdefghijklmnopqrstuvwxyz";


        for (i = 0; i = Math.ceil(alphabet.length / 3); i++) {
            for (n = 0; n < 3; n++) {

                switch (n) {
                    case 0:
                        var tempLetter = alphabet.substring(n, n + 1);
                        zeroletters.push(tempLetter);
                        break;
                    case 1:
                        var tempLetter = alphabet.substring(n, n + 1);
                        oneletters.push(tempLetter);
                        break;
                    case 2:
                        var tempLetter = alphabet.substring(n, n + 1);
                        twoletters.push(tempLetter);
                        break;
                }


            }
            alphabet = alphabet.substring(3);
            console.log(alphabet);
        }

        tempArray = [];

        for (let i of twoletters)
            i && tempArray.push(i); // copy each non-empty value to the 'temp' array

        twoletters = tempArray;
        delete tempArray;




        console.log(zeroletters);
        console.log(oneletters);
        console.log(twoletters);
    };

    divAlphabet();

    // Now coding for adding apples download or pineapple image according to if the word starts with one of the letters out of the arrays that come out of the divAlphabet function.
    // ["a", "d", "g", "j", "m", "p", "s", "v", "y"]
    //
    // ["b", "e", "h", "k", "n", "q", "t", "w", "z"]
    //
    // ["c", "f", "i", "l", "o", "r", "u", "x"]

    function findSelectorforselectImageAccordingToLetter(inputWord, parentNodeElement) {
        var firstLetter = inputWord.substring(0, 1);
        var parentNodeElement = parentNodeElement;
        // console.log(firstLetter);
        // console.log(oneletters);

        function checkInWhichArray(firstLetter) {
            // console.log(firstLetter);
            for (i = 0; i < zeroletters.length; i++) {

                // console.log(zeroletters[i]);
                tempZeroString = zeroletters[i];
                if (firstLetter.toLowerCase() == tempZeroString.toLowerCase()) {
                    selectImageAccordingToLetter(0, parentNodeElement);

                    return;
                }

            };
            for (i = 0; i < oneletters.length; i++) {

                // console.log(zeroletters[i]);
                tempZeroString = oneletters[i];
                if (firstLetter.toLowerCase() == tempZeroString.toLowerCase()) {
                    selectImageAccordingToLetter(1, parentNodeElement);
                    return;
                }

            };
            selectImageAccordingToLetter(2, parentNodeElement);
            return;
        };

        checkInWhichArray(firstLetter);

    }


    function selectImageAccordingToLetter(caseInput, parentNodeElement) {
        parentNodeElementImg = parentNodeElement.querySelector("img");
        console.log(parentNodeElementImg);
        if (caseInput == 0) {
            // parentNode.querySelector("img").src = "img/download.jpg";
            parentNodeElementImg.src = "img/apples.jpg";
        } else if (caseInput == 1) {
            parentNodeElementImg.src = "img/download.jpg";
        } else {
            parentNodeElementImg.src = "img/pineapple.jpg";
        }

    }





};
